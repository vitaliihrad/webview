package vitaliihrad.webviewtest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import vitaliihrad.webviewtest.databinding.FragmentTermsUserBinding
import kotlin.system.exitProcess

class TermsUserFragment : Fragment() {

    private lateinit var binding: FragmentTermsUserBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTermsUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        binding.apply {
            exitBtn.setOnClickListener {
                exitProcess(-1)
            }
            agreeBtn.setOnClickListener {
                parentFragmentManager.commit {
                    replace(R.id.main_container, WebViewFragment.newInstance(), WebViewFragment.TAG)
                }
            }
        }
    }

    companion object {
        val TAG: String = TermsUserFragment::class.java.name

        fun newInstance() = TermsUserFragment()
    }
}