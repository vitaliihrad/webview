package vitaliihrad.webviewtest

import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.appsflyer.AppsFlyerLib
import com.appsflyer.attribution.AppsFlyerRequestListener
import com.onesignal.OneSignal

const val ONESIGNAL_APP_ID = "90715435-83c5-4165-b202-89b8b431e164"
const val DEV_KEY_APPS_FLYER = "ZvnfYiiXffQwAavPXa6LQJ"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setAppsFlyerLib()
        setOneSignal()
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.main_container, TermsUserFragment.newInstance(), TermsUserFragment.TAG)
            }
        }
    }

    private fun setAppsFlyerLib() {
        AppsFlyerLib.getInstance().init(DEV_KEY_APPS_FLYER, null, this)
        AppsFlyerLib.getInstance().start(this)

        AppsFlyerLib.getInstance().start(this, DEV_KEY_APPS_FLYER, object :
            AppsFlyerRequestListener {
            override fun onSuccess() {
                Log.d("AppsFlyerLib", "Launch sent successfully")
            }

            override fun onError(errorCode: Int, errorDesc: String) {
                Log.d(
                    "AppsFlyerLib", "Launch failed to be sent:\n" +
                            "Error code: " + errorCode + "\n"
                            + "Error description: " + errorDesc
                )
            }
        })
        AppsFlyerLib.getInstance().setDebugLog(true)
    }

    private fun setOneSignal() {
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        OneSignal.initWithContext(this)
        OneSignal.setAppId(ONESIGNAL_APP_ID)
    }

    override fun onBackPressed() {
        val webView = findViewById<WebView>(R.id.test_web_view)
        if (WebViewFragment.isActive) {
            if (webView.canGoBack()) webView.goBack() else super.onBackPressed()
        } else super.onBackPressed()
    }
}

