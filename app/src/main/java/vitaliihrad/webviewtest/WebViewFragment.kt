package vitaliihrad.webviewtest

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import vitaliihrad.webviewtest.databinding.FragmentWebViewBinding

class WebViewFragment : Fragment() {

    private lateinit var binding: FragmentWebViewBinding
    private val webView by lazy { binding.testWebView }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWebViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isActive = true
        webViewSetup(savedInstanceState, getString(R.string.base_url))
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun webViewSetup(savedInstanceState: Bundle?, url: String) {
        webView.webViewClient = WebViewClient()
        webView.apply {
            if (savedInstanceState == null) {
                loadUrl(url)
            } else {
                restoreState(savedInstanceState)
            }
            settings.javaScriptEnabled = true
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView.saveState(outState)
    }

    companion object {
        val TAG: String = WebViewFragment::class.java.name
        var isActive: Boolean = false

        fun newInstance() = WebViewFragment()
    }
}